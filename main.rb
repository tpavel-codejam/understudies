t = gets.to_i

1.upto(t) do |i|
  n = gets.to_i

  probabilities = gets.split(' ').map(&:to_f)

  # sort them probabilities
  probabilities.sort!

  primaries = probabilities.first(n)
  understudies = probabilities.last(n).reverse

  role_probabilities = primaries
                       .zip(understudies)
                       .map { |a, b| 1 - a * b }

  result = format('%.6f', role_probabilities.reduce(&:*))

  puts "Case ##{i}: #{result}"
end
